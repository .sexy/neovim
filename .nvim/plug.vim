call plug#begin('~/.nvim/plugged')

" Plug 'Valloric/YouCompleteMe'
" Plug 'JessicaKMcIntosh/TagmaBufMgr'
" Plug 'vim-scripts/taglist.vim'
" Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
" Plug 'L9'
" Plug 'FuzzyFinder'
" Plug 'wincent/Command-t'
" Plug 'majutsushi/tagbar'
" Plug 'altercation/vim-colors-solarized'
Plug 'https://gitlab.com/sexy-zsh/solarized-darcula-color-scheme.git', { 'rtp': 'vim' }
" Plug 'vim-scripts/colorsupport.vim'
" Plug 'mhinz/vim-startify'

Plug 'https://gitlab.com/sexy-zsh/minibufexpl.vim.git'

Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Xuyuanp/nerdtree-git-plugin'

Plug 'Shougo/neocomplcache.vim'
Plug 'Lokaltog/vim-easymotion'
Plug 'xolox/vim-easytags'
Plug 'xolox/vim-misc'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'bling/vim-airline'
Plug 'jordwalke/flatlandia'
Plug 'airblade/vim-gitgutter'

call plug#end()

