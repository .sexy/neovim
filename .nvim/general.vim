let data_dir   = "~/.data/"
let backup_dir = "~/.data/backup/"
let swap_dir   = "~/.data/swap/"

if finddir(data_dir) == ''
    silent call mkdir(data_dir)
endif
if finddir(backup_dir) == ''
    silent call mkdir(backup_dir)
endif
if finddir(swap_dir) == ''
    silent call mkdir(swap_dir)
endif

set backupdir=~/.data/backup
set directory=~/.data/swap

unlet data_dir
unlet backup_dir
unlet swap_dir

map Q gq                

inoremap <C-U> <C-G>u<C-U>
filetype plugin indent on



